﻿using System;
namespace dotnetcore
{
    public class RainingLetter : GameTools.GameEngine
    {
        private MyFirstProgram.MatrixRepresentation _matrix;
        protected override  void Start()
        {
           _matrix = new MyFirstProgram.MatrixRepresentation();
            var matrixOf0 = new char[9, 9];
            for (var y = 0; y < matrixOf0.GetLength(0); y++)
                for (var x = 0; x < matrixOf0.GetLength(1); x++)
                    matrixOf0[y, x] = '0';
            _matrix.clippingMatrix(matrixOf0);
        }

        protected override void UpdateGame()
        {
            do
            {
                while (Console.KeyAvailable == false)
                {

                    CleanFrame();

                    Update();

                    RefreshFrame();

                    Frames++;
                }

                cki = Console.ReadKey(true);
                CheckKeyboard4Engine();
            } while (!engineSwitch);
        }
        protected override void Update()
        {
            char[] randomchar = new char[] { 'a', 'b', 'c', 'd', 'e', 'f' };
            Random rnd = new Random();
            _matrix.printMatrix();
            if (Frames != 0)
                for (var x = 0; x < _matrix.TheMatrix.GetLength(1); x++)
                {
                    for (var y = _matrix.TheMatrix.GetLength(0) - 1; y >= 0 && _matrix.TheMatrix[y, x] != 0; y--)
                    {
                        if (y != _matrix.TheMatrix.GetLength(0) - 1 && _matrix.TheMatrix[y + 1, x] == '0')
                            (_matrix.TheMatrix[y, x], _matrix.TheMatrix[y + 1, x]) = (_matrix.TheMatrix[y + 1, x], _matrix.TheMatrix[y, x]);
                        else _matrix.TheMatrix[y, x] = '0';
                    }
                }
            for (var num = 0; num < 2; num++)
                _matrix.TheMatrix[0, rnd.Next(0, 9)] = randomchar[rnd.Next(0, 6)];
        }

        protected override void Exit()
        {
            System.Threading.Thread.Sleep(1000);
            Console.Clear();
            _matrix.blankMatrix();
        }
    }
}
