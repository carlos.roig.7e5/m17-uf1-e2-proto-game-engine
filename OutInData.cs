﻿using System;
using System.Collections.Generic;
using System.Text;

namespace dotnetcore
{
    public class OutInData
    {
        public static int InPosInteger(string missatge)
        {
            Console.WriteLine(missatge);
            bool IsInt;
            int PosInt;
            do
            {
                IsInt = int.TryParse(Console.ReadLine(), out PosInt);
                if (!IsInt || PosInt < 0)
                    Console.WriteLine("Parametro Incorrecto");
            } while (!IsInt || PosInt < 0);
            return PosInt;
        }

        public static void ClearLastLine()
        {
            Console.SetCursorPosition(Console.CursorLeft, Console.CursorTop - 1);
        }

        public static void Flecha(int x)
        {
            Console.SetCursorPosition(Console.CursorLeft + x*2 + 1, Console.CursorTop);
            Console.WriteLine("▲");
        }
    }
}
