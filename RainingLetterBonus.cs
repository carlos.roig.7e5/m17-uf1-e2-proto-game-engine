﻿using System;

namespace dotnetcore
{
   public class RainingLetterBonus : GameTools.GameEngine
    {
        private MyFirstProgram.MatrixRepresentation _matrix;
        private int _matrixColumn;
        private ConsoleKeyInfo select;
        protected override void Start()
        {
            CreateMatrixOf0();
        
           
        }

        private void CreateMatrixOf0()
        {
            _matrix = new MyFirstProgram.MatrixRepresentation();
            var matrixOf0 = new char[9, 9];
            for (var y = 0; y < matrixOf0.GetLength(0); y++)
                for (var x = 0; x < matrixOf0.GetLength(1); x++)
                    matrixOf0[y, x] = '0';
            _matrix.clippingMatrix(matrixOf0);
            SelectColumnMethod();
        }
        
        private void SelectColumnMethod()
        {
            switch (OutInData.InPosInteger("Select the column: \n [0] By keywoard. \n [1] By arrows."))
            {
                case 0:
                    SelecColumnByPrint();
                    break;
                case 1:
                   SelectColumnByArrow();
                    break;
            }
        }
        private void SelectColumnByArrow()
        {
            Console.WriteLine("Select the column you want using the key arrows and then click enter : ");
            bool sortir;
            do
            {
                Console.WriteLine("<- " + _matrixColumn + " ->");
                sortir = false;
                do
                {
                    select = Console.ReadKey();
                } while (select.Key != ConsoleKey.RightArrow && select.Key != ConsoleKey.LeftArrow && select.Key != ConsoleKey.Enter);
                switch (select.Key)
                {
                    case ConsoleKey.RightArrow:
                        _matrixColumn++;
                        break;
                    case ConsoleKey.LeftArrow:
                        _matrixColumn--;
                        break;
                    default:
                        sortir = true;
                        break;
                }
                if (_matrixColumn >= 9) _matrixColumn = 0;
                if (_matrixColumn < 0) _matrixColumn = 8;
                OutInData.ClearLastLine();
            } while (!sortir);
        }

        private void SelecColumnByPrint()
        {
            do
            {
                _matrixColumn = OutInData.InPosInteger("Print the column you want:");
            } while (_matrixColumn >= 9);
        }

        protected override void UpdateGame()
        {
            do
            {
                while (Console.KeyAvailable == false)
                {

                    Update();

                    RefreshFrame();

                    Frames++;
                }
                cki = Console.ReadKey(true);
                CheckKeyboard4Engine();
            } while (!engineSwitch);
        }

        protected override void Update()
        {
            char[] randomchar = new char[] { 'a', 'b', 'c', 'd', 'e', 'f' };
            Random rnd = new Random();

            
            _matrix.printMatrix();
            if (Frames != 0)
                for (var x = 0; x < _matrix.TheMatrix.GetLength(1); x++)
                {
                    for (var y = _matrix.TheMatrix.GetLength(0) - 1; y >= 0 && _matrix.TheMatrix[y, x] != 0; y--)
                    {
                        if (y != _matrix.TheMatrix.GetLength(0) - 1 && _matrix.TheMatrix[y + 1, x] == '0')
                            (_matrix.TheMatrix[y, x], _matrix.TheMatrix[y + 1, x]) = (_matrix.TheMatrix[y + 1, x], _matrix.TheMatrix[y, x]);
                        else _matrix.TheMatrix[y, x] = '0';
                    }
                }
          _matrix.TheMatrix[0, _matrixColumn] = randomchar[rnd.Next(0, 6)];

            OutInData.Flecha(_matrixColumn);
            if (Console.KeyAvailable)
                select = Console.ReadKey();
            switch (select.Key)
            {
                case ConsoleKey.RightArrow:
                    _matrixColumn++;
                    break;
                case ConsoleKey.LeftArrow:
                    _matrixColumn--;
                    break;
            }
            select = new ConsoleKeyInfo();
            if (_matrixColumn >= 9) _matrixColumn = 0;
            if (_matrixColumn < 0) _matrixColumn = 8;
        }

        protected override void Exit()
        {
            System.Threading.Thread.Sleep(1000);
            Console.Clear();
            _matrix.blankMatrix();
        }
    }
}
